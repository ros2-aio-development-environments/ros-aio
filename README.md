# Development Containers: microROS + ROS + Gazebo

This repository is a scaffolding encompassing the development containers and code workspaces for microROS and ROS.
The repository consists of two major submodules for ros and micro ros deveopment environments respectively. Each development environment ccontains a folder named workspaces which houses ros/micro-ros code workspaces. The project structure is as shown below. The alphanumeric sequence provided in parenthesis denotes the organization of repos. Numbers indicate main repos and letters indicate submodules.

```
ROS-AIO                               # All in one main repo (1)
	├── .devcontainer
	├── micro-ros-environment         # Micro Ros Environment Submodule (1A)
	│   ├── .devcontainer
	│   └── workspaces                # Micro Ros Code workspaces 
	│   	├── workspace #1          # Workspace main repo 	(1A1)		
	│   	│	├── package #1        # Package in workspace repo (1A1A)
	│   	│	├── package #2
	│   	│	├── ...
	│   	│	└── package #n
	│   	├── workspace #2
	│		├── ...
	│		└── workspace #n
	├── ros-environment               # Ros Environment Submodule (1B)
	│   ├── .devcontainer
	│   ├── app-scripts
	│   ├── envs
	│   └── workspaces                # Ros Code workspaces 
	│   	├── workspace #1          # Single workspace main repo 	(1B1)		
	│   	│	├── package #1        # Package in workspace repo (1B1B) 
	│   	│	├── package #2
	│   	│	├── ...
	│   	│	└── package #n
	│   	├── workspace #2
	│		├── ...
	│		└── workspace #n
	└── README.md
```

## Setup

To clone the ros and micro ros repositories, run the git clone command with the `--recursive` argument. The submodules will set the stage for micro-ros and ros development. Open the root ROS-AIO folder as remote container in VSCode to open up 2 VSCode windows, one connected to ros container and the second connected to micro-ros container, simultaneously. If only one environment is required, you can open only the corresponding environment in VSCode. 

Once the stage has been set, you can clone in the required workspaces in the opened remote container environment for ros development with the required package git submodule.

## License

Dummy License